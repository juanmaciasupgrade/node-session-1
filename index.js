const express = require("express");

const PORT = 4000;
const app = express();

// Creamos un router para express
const router = express.Router();

// Configuramos una ruta, en este caso "/"" equivale a http://localhost:4000
router.get("/", (request, response) => {
  return response.send("Hola mundo! Nuestro servidor funciona");
});

// endpoint -> cada ruta de nuestro servidor
router.get("/movies", (req, res) => {
  const movies = ["Harry Potter", "Los goonies", "Wall Street", "Gladiator", "Gremlins"];

  return res.send(movies);
});

router.get("/movies/:pelicula/:year", (req, res) => {
  // Programaremos esta ruta de manera que se puedan buscar películas individualmente.
  // Si el usuario busca /movies/titanic -> le diremos que no existe en nuestro catálogo
  // Si el usuario busca /movies/gladiator -> le diremos que si existe.
  const movies = ["Harry Potter", "Los goonies", "Wall Street", "Gladiator", "Gremlins"];

  const { pelicula } = req.params;

  const finded = movies.find((movie) => movie.toLowerCase() === pelicula.toLowerCase());

  if (finded) {
    return res.send(`Película encontrada, aquí la tienes: ${finded}`);
  } else {
    return res.send(`No tenemos la película que buscas. ${pelicula} no está en nuestra colección`);
  }
});

// Le decimos a express que use nuestro router
app.use("/", router);

// Creamos nuestro servidor
app.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});

/**
 * 1. npm init -y
 * 2. npm install express
 * 3. crear archivo index.js
 * 4. en index.js añadir el código de este archivo.
 */

/**
 * ¿Qué es lo que tenemos que hacer en node?
 * Gestionar todas las peticiones que nos entren.
 *
 * La finalidad es darle una salida a cada petición.
 * - Salida correcta
 * - Error durante el camino.
 */
